FROM ubuntu:focal

LABEL maintainer="Jeffrey Steward <nulifier@gmail.com>"

# Version number
ARG GODOT_VERSION=3.3

# Install Dependencies
RUN apt update \
    && apt install -y wget unzip python scons mingw-w64 \
    && rm -rf /var/lib/apt/lists/*

# Install Godot
RUN wget \
        "http://download.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip" \
        "http://download.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_export_templates.tpz" \
    && unzip Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip \
    && mv Godot_v${GODOT_VERSION}-stable_linux_headless.64 /usr/local/bin/godot \
    && mkdir -p ~/.local/share/godot/templates/${GODOT_VERSION}.stable \
    && mkdir -p ~/.cache \
    && mkdir -p ~/.config/godot \
    && unzip Godot_v${GODOT_VERSION}-stable_export_templates.tpz \
    && mv templates/* ~/.local/share/godot/templates/${GODOT_VERSION}.stable \
    && rm -rf templates Godot_v${GODOT_VERSION}-stable_*
